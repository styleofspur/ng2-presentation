import './polyfills.ts';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode }         from '@angular/core';
import { environment }            from './environments/environment';
import { hmrBootstrap }           from './hmr';
import { AppModule }              from './app/';

/**
 * Every Angular app has at least one Angular module class, the root module, conventionally named AppModule.
 *
 * DI
 * We don't have to create an Angular injector.
 * Angular creates an application-wide injector for us during the bootstrap process.
 */
const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule);

if (environment.production) {
  enableProdMode();
} else {
  if (module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    console.error('HMR is not enabled for webpack-dev-server!');
    console.log('Are you using the --hmr flag for ng serve?');
  }
}
