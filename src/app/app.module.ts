import { BrowserModule }    from '@angular/platform-browser';
import { NgModule }         from '@angular/core';

/* routing */
import { AppRoutingModule } from './app-routing.module';

/* components */
import { AppComponent }        from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';

/* pages */
import { DirectivesModule } from './pages/directives/directives.module';
import { ShareDataModule}   from './pages/share-data/share-data.module';
import { TemplateModule }   from './pages/template/template.module';
import { ServicesModule }   from './pages/services/services.module';
import {
  FormsPageModule as FormsModule
} from './pages/forms/forms.module';
import { LifecycleModule } from './pages/lifecycle/lifecycle.module';
import { PipesModule }     from './pages/pipes/pipes.module';
import { I18nModule }      from './pages/i18n/i18n.module';
import { SecurityModule }  from './pages/security/security.module';
import { HttpModule }      from './pages/http/http.module';
import { ReduxModule }     from './pages/redux/redux.module';

/* services */
import { EverywhereAvailableService } from './services/everywhere-available/everywhere-available.service';
import { SecurityComponent }          from './pages/security/security.component';

@NgModule({

  /*
    The view classes that belong to this module.
    Angular has three kinds of view classes: components, directives, and pipes.
  */
  declarations: [
    // components
    AppComponent,
    NavigationComponent,
    SecurityComponent
  ],

  /*
    The subset of declarations that should be visible and usable in the component templates of other modules
  */
  // exports: [],

  /*
    Other modules whose exported classes are needed by component templates declared in this module.
   */
  imports: [
    BrowserModule, // every application needs it to run in a browser
    AppRoutingModule,

    DirectivesModule, // directives page
    TemplateModule,   // template page
    ShareDataModule,  // about page
    ServicesModule,   // services page
    FormsModule,      // forms page
    LifecycleModule,  // lifecycle page
    PipesModule,      // pipes page
    I18nModule,       // i18n page
    SecurityModule,   // security page
    HttpModule,       // http page
    ReduxModule,      // redux page
  ],

  /*
    Creators of services that this module contributes to the global collection of services.
    They become accessible in all parts of the app.

    Here we configure the injector by registering the providers that create the services our application requires.

    Every provider registered within the root module (= within the root injector) will be accessible in the entire application.
    On the other hand, a provider registered in an application component is available only on that component and all its children.
   */
  providers: [
      EverywhereAvailableService // entire app accessible service
  ],

  /*
    Each bootstrapped component is the base of its own tree of components.
    Inserting a bootstrapped component usually triggers a cascade of component creations that fill out that tree.

    While we can put more than one component tree on a host web page, that's not typical.
    Most applications have only one component tree and they bootstrap a single root component.

    We can call the one root component anything we want but most developers call it AppComponent.
    This is the root component that Angular creates and inserts into the index.html host web page
  */
  bootstrap: [AppComponent]
})
export class AppModule { }
