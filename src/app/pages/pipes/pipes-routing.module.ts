import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PipesComponent } from './pipes.component';

const rootRouteConfig = {
  path:      'pipes',
  component: PipesComponent,
  data: {
    title: 'Pipes Page'
  }
};
const routes: Routes = [
  rootRouteConfig
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PipesRoutingModule { }
