import { Component }              from '@angular/core';
import { ActivatedRoute }         from '@angular/router';
import { Title }                  from '@angular/platform-browser';
import { Observable, Subscriber } from 'rxjs';

import { Page } from '../page.class';

@Component({
  selector:    'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls:   [ './pipes.component.sass' ]
})
export class PipesComponent extends Page {

  /**
   * @type {string}
   * @protected
   */
  protected _title: string = 'Pipes';

  /**
   * @type {boolean}
   * @protected
   */
  protected _toggle: boolean = true;

  /**
   * @public
   * @type {Object}
   */
  public model = {
    builtIn: {
      date:      Date.now(),
      currency:  10,
      uppercase: 'hello',
      lowercase: 'HELLO',
      percent:   0.25,
      async:     new Observable<string>((observer: Subscriber<string>) => {
        setInterval(() => observer.next(new Date().toString()), 1000);
      })
    }
  };

  /**
   * @public
   * @returns {string}
   */
  public get format(): string {
    return this._toggle ? 'shortDate' : 'fullDate';
  }

  /**
   * @constructor
   * @param {ActivatedRoute} route
   * @param {Title} title
   */
  constructor(route?: ActivatedRoute, title?: Title) {
    super(route, title);
  }

  /**
   * @public
   * @returns void
   */
  public toggleFormat(): void {
    this._toggle = !this._toggle;
  }

}
