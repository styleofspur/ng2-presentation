import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipes transform displayed values within a template.
 * Here are the requirements for pipe class:
 *
 *  1. a pipe is a class decorated with pipe metadata.
 *  2. pipe class implements PipeTransform interface's transform method that accepts input value followed by optional parameters and returns transformed value.
 *  3. there will be one additional argument to the transform method for each parameter passed to the pipe. Our pipe has one such parameter: the precision.
 *  4. we tell Angular that this is a pipe by applying the @Pipe decorator which we import from the core Angular library.
 *
 * The @Pipe decorator allows us to define the pipe name that we'll use within template expressions.
 * It must be a valid JavaScript identifier. Our pipe's name is round.
 *
 */

/**
 * Rounds the value.
 * Default precision is 1 and max is 21 as due to the API of .toPrecision([precision]) method.
 * See https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_objects/Number/toPrecision).
 *
 * Usage:
 *    value | round:precision
 *
 * Example:
 *    {{ 3.14 | round }}
 *    outputs 3
 *
 *    {{ 3.14 | round:2 }}
 *    outputs 3.1
 */
@Pipe({
  name: 'round',

  /*

    There are two categories of pipes: pure and impure.
    Pipes are pure by default. Every pipe we've seen so far has been pure. We make a pipe impure by setting its pure flag to false.

      Pure pipes (https://angular.io/docs/ts/latest/guide/pipes.html#!#pure-pipes)
    Angular executes a pure pipe only when it detects a pure change to the input value.
    A pure change is either a change to a primitive input value (String, Number, Boolean, Symbol) or a changed object reference (Date, Array, Function, Object).

    Angular ignores changes within (composite) objects.
    It won't call a pure pipe if we add to an input array or update an input object property.

    This may seem restrictive but is is also fast.
    An object reference check is fast — much faster than a deep check for differences —
    so Angular can quickly determine if it can skip both the pipe execution and a view update.

    For this reason, we prefer a pure pipe if we can live with the change detection strategy. When we can't, we may turn to the impure pipe.

      Impure pipes (https://angular.io/docs/ts/latest/guide/pipes.html#!#impure-pipes)
    Angular executes an impure pipe during every component change detection cycle.
    An impure pipe will be called a lot, as often as every keystroke or mouse-move.

   */
  // pure: true
})
export class RoundPipe implements PipeTransform {

  /**
   * @type {number}
   * @private
   */
  private _defaultPrecision: number = 1;

  /**
   * @type {number}
   * @private
   */
  private _maxPrecision: number = 21;

  /**
   * @public
   * @returns {string}
   */
  public toString(): string {
    return `#RoundPipe`;
  }

  /**
   * The transform method is essential to a pipe.
   * The PipeTransform interface defines that method and guides both tooling and the compiler.
   * It is technically optional; Angular looks for and executes the transform method regardless.
   *
   * @public
   * @param {number|string} value
   * @param {number} precision
   * @throws {Error} when the precision param is greater than max one.
   * @returns {number}
   */
  public transform(value: number|string, precision: number = this._defaultPrecision): any {
    if (typeof value === 'string') {
      value = parseFloat(value);
    }
    if (precision > this._maxPrecision) {
      throw new Error(`${this}: provided "precision" value ${precision} is greater that max one ${this._maxPrecision}`);
    }

    return +(value.toPrecision(precision));
  }

}
