import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';

import { PipesComponent } from './pipes.component';
import { RoundPipe }      from './pipes/round/round.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
      PipesComponent,

      // pipes
      RoundPipe
  ]
})
export class PipesModule { }
