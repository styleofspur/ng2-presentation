import { Component }      from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title }          from '@angular/platform-browser';
import { Observable }     from 'rxjs';
import { select }         from 'ng2-redux';

import { Page }           from '../../page.class';
import { CounterActions } from './actions/counter.actions';

/**
 * Example is created with https://github.com/angular-redux/ng2-redux.
 * Based on https://plnkr.co/edit/Ci7RDJPIcu43AD3zSZ1O
 */
@Component({
  selector:    'app-ng2-redux',
  providers: [
      CounterActions
  ],
  templateUrl: './ng2-redux.component.html',
  styleUrls:   ['./ng2-redux.component.sass']
})
export class Ng2ReduxComponent extends Page {

  /**
   * @type {string}
   * @protected
   */
  protected _title: string = 'ng2-redux page';

  /**
   * @public
   * @type {string}
   */
  public text: string = 'Ng2Redux';

  /**
   * @public
   * @type {Observable<number>}
   */
  @select()
  public counter$: Observable<number>;

  /**
   *
   * @public
   * @constructor
   * @param {CounterActions} counterActions
   * @param {ActivatedRoute} route
   * @param {Title}          title
   */
  public constructor(private counterActions?: CounterActions, route?: ActivatedRoute, title?: Title) {
    super(route, title);
  }

}
