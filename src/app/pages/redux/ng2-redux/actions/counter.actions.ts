import { Injectable } from '@angular/core';
import { NgRedux }    from 'ng2-redux';

export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';

@Injectable()
export class CounterActions {

    /**
     *
     * @param {number} timeInMs
     * @return {Promise<any>}
     * @private
     */
    private _delay(timeInMs: number): Promise<any> {
        return new Promise(resolve => {
            setTimeout(() => resolve(), timeInMs);
        });
    }

    /**
     *
     * @public
     * @constructor
     * @param {NgRedux} redux
     */
    public constructor(private redux: NgRedux<any>) {}

    /**
     *
     * @public
     * @return {void}
     */
    public increment(): void {
        this.redux.dispatch({ type: INCREMENT_COUNTER });
    }

    /**
     *
     * @public
     * @return {void}
     */
    public decrement() {
        this.redux.dispatch({ type: DECREMENT_COUNTER });
    }

    /**
     *
     * @public
     * @return {void}
     */
    public incrementIfOdd(): void {
        const { counter } = this.redux.getState();

        if (counter % 2 === 0) {
            return;
        }
        this.redux.dispatch({ type: INCREMENT_COUNTER });
    }

    /**
     *
     * @public
     * @return {void}
     */
    public incrementAsync(timeInMs: number = 1000): void {
        this._delay(timeInMs).then(() => this.redux.dispatch({ type: INCREMENT_COUNTER }));
    }
}
