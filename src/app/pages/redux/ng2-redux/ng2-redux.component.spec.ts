/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Component }              from '@angular/core';
import { Location }               from '@angular/common';
import { Router }                 from '@angular/router';
import { RouterTestingModule }    from '@angular/router/testing';

import { Ng2ReduxComponent } from './ng2-redux.component';

@Component({
  template: `
    <router-outlet></router-outlet>
  `
})
class RoutingComponent { }

describe('Ng2ReduxComponent', () => {

  describe('render', () => {

    let router, location;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [
          RoutingComponent,
          Ng2ReduxComponent
        ],
        imports: [
          RouterTestingModule.withRoutes([
            { path: 'redux', component: Ng2ReduxComponent, data: { title: 'Redux Page'} }
          ])
        ]
      });
    });

    beforeEach(inject([Router, Location], (_router: Router, _location: Location) => {
      location = _location;
      router   = _router;
    }));

    it('should create the Redux page', async(() => {
      let fixture = TestBed.createComponent(Ng2ReduxComponent);
      let app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    }));

    it(`should have as title 'Redux'`, async(() => {
      let fixture = TestBed.createComponent(Ng2ReduxComponent);
      let app = fixture.debugElement.componentInstance;
      expect(app.text).toEqual('Redux');
    }));

    it('should set page title with default value', async(() => {
      let fixture = TestBed.createComponent(Ng2ReduxComponent);
      fixture.detectChanges();
      expect(document.title).toBe('redux page');
    }));

    it('should set page title with router data value', async(() => {
      let fixture = TestBed.createComponent(RoutingComponent);
      fixture.detectChanges();
      router.navigate(['/redux']).then(() => {
        expect(location.path()).toBe('/redux');
        expect(document.title).toBe('Redux Page');
      });
    }));

  });

  describe('instance', () => {

    it('should have `text` property with string type and default value', () => {
      let instance = new Ng2ReduxComponent();
      expect(instance.text).toBeDefined();
      expect(typeof instance.text).toBe('string');
      expect(instance.text).toBe('Redux');
    });

  });

});
