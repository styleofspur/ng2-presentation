import { NgModule }               from '@angular/core';
import { CommonModule }           from '@angular/common';
import { NgReduxModule, NgRedux, DevToolsExtension } from 'ng2-redux';

import { Ng2ReduxComponent } from './ng2-redux.component';
import {
  CounterComponent as Ng2ReduxCounterComponent
} from './components/counter/counter.component';
import rootReducer from './reducers';

@NgModule({
  declarations: [
    Ng2ReduxComponent,
    Ng2ReduxCounterComponent
  ],
  imports: [
    CommonModule,
    NgReduxModule
  ],
  bootstrap: [Ng2ReduxComponent]
})
export class Ng2ReduxModule {

  /**
   *
   * @public
   * @constructor
   * @param {NgRedux}           _ngRedux
   * @param {DevToolsExtension} _devTools
   */
  public constructor(
      private _ngRedux:  NgRedux<any>,
      private _devTools: DevToolsExtension
  ) {
    const enhancers: Array<any> = [ _devTools.enhancer() ];

    _ngRedux.configureStore(
        rootReducer,
        {}, // initial state
        [], // middlewares
        enhancers
    );
  }

}
