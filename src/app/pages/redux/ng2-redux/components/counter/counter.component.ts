import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable }                             from 'rxjs';

@Component({
    selector: 'app-counter',
    template: `
      <p>
        Clicked: {{ counter | async }} times
        <button (click)="increment.emit()">+</button>
        <button (click)="decrement.emit()">-</button>
        <button (click)="incrementIfOdd.emit()">Increment if odd</button>
        <button (click)="incrementAsync.emit()">Increment async</button>
      </p>
    `
})
export class CounterComponent {

    /**
     * @public
     * @type {Observable<number>}
     */
    @Input()
    public counter: Observable<number>;

    /**
     * @public
     * @type {EventEmitter<void>}
     */
    @Output()
    public increment = new EventEmitter<void>();

    /**
     * @public
     * @type {EventEmitter<void>}
     */
    @Output()
    public decrement = new EventEmitter<void>();

    /**
     * @public
     * @type {EventEmitter<void>}
     */
    @Output()
    public incrementIfOdd = new EventEmitter<void>();

    /**
     * @public
     * @type {EventEmitter<void>}
     */
    @Output()
    public incrementAsync = new EventEmitter<void>();
}
