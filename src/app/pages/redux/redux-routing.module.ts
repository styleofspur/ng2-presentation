import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReduxComponent }    from './redux.component';
import { Ng2ReduxComponent } from './ng2-redux/ng2-redux.component';

const rootRouteConfig = {
  path: 'redux',
  component: ReduxComponent,
  data: {
    title: 'Redux'
  }
};
const routes: Routes = [
  rootRouteConfig,
  {
    path: 'redux/ng2-redux',
    component: Ng2ReduxComponent,
    data: {
      title: 'Ng2 Redux'
    }
  },
  // {
  //   path: 'ngrx',
  //   component: NgRxComponent,
  //   data: {
  //     title: 'NgRx'
  //   }
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ReduxRoutingModule { }
