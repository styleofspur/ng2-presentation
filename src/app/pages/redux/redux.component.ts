import { Component }      from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title }          from '@angular/platform-browser';

import { Page } from '../page.class';

@Component({
  selector:    'app-redux-page',
  templateUrl: './redux.component.html',
  styleUrls:   ['./redux.component.sass']
})
export class ReduxComponent extends Page {

  /**
   * @type {string}
   * @protected
   */
  protected _title: string = 'redux page';

  /**
   * @public
   * @type {string}
   */
  public text: string = 'Redux';

  /**
   * @constructor
   * @param {ActivatedRoute} route
   * @param {Title}          title
   */
  constructor(route?: ActivatedRoute, title?: Title ) {
    super(route, title);
  }

}
