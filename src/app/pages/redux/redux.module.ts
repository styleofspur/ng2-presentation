import { NgModule } from '@angular/core';

import { ReduxRoutingModule } from './redux-routing.module';
import { ReduxComponent }     from './redux.component';
import { Ng2ReduxModule }     from './ng2-redux/ng2-redux.module';

/**
 * Intro to Angular2 Redux can be found here: https://angular-2-training-book.rangle.io/handout/redux/
 */
@NgModule({
  declarations: [
    ReduxComponent
  ],
  imports: [
    Ng2ReduxModule,

    ReduxRoutingModule
    // NgRxModule
  ],
  bootstrap: [ ReduxComponent ]
})
export class ReduxModule { }
