import { Component }      from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title }          from '@angular/platform-browser';

import { Page } from '../page.class';

@Component({
  selector:    'app-i18n',
  templateUrl: './i18n.component.html',
  styleUrls:   [ './i18n.component.sass' ]
})
export class I18nComponent extends Page {

  /**
   *
   * @param {ActivatedRoute} route
   * @param {Title} title
   */
  constructor(route?: ActivatedRoute, title?: Title) {
    super(route, title);
  }

}
