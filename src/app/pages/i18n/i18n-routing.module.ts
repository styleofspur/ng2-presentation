import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { I18nComponent } from './i18n.component';

const rootRouteConfig = {
  path:      'i18n',
  component: I18nComponent,
  data: {
    title: 'I18n Page'
  }
};
const routes: Routes = [
  rootRouteConfig
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class I18nRoutingModule { }
