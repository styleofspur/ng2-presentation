import { NgModule }     from '@angular/core';
import {
  FormsModule,
  ReactiveFormsModule
}  from '@angular/forms';
import { CommonModule } from '@angular/common';

import { FormsComponent }                  from './forms.component';
import { FormBuilderComponent }            from './components/form-builder/form-builder.component';
import { ValidateIsNotTestDirective }      from './directives/validate-is-not-test/validate-is-not-test.directive';
import { ValidateIsNotTestAsyncDirective } from './directives/validate-is-not-test-async/validate-is-not-test-async.directive';
import { CounterInputComponent }           from './components/counter-input/counter-input.component';

@NgModule({
  declarations: [
    FormsComponent,
    FormBuilderComponent,
    ValidateIsNotTestDirective,
    ValidateIsNotTestAsyncDirective,
    CounterInputComponent,
  ],
  imports: [
    FormsModule,
    // necessary for formGroup, formGroupName, formControl, formControlName etc. directives
    ReactiveFormsModule,
    CommonModule
  ],
  bootstrap: [FormsComponent]
})
export class FormsPageModule { }
