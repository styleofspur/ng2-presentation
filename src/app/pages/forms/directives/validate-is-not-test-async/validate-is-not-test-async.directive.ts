import { Directive }                        from '@angular/core';
import { FormControl, NG_ASYNC_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appValidateIsNotTestAsync]',
  providers: [
    // with multi providers, we can provide multiple values for a single token in DI
    { provide: NG_ASYNC_VALIDATORS, useExisting: ValidateIsNotTestAsyncDirective, multi: true}
  ]
})
export class ValidateIsNotTestAsyncDirective {

  constructor() { }

  /**
   * In order to make this method work on each form control change we've provided NG_ASYNC_VALIDATORS above
   *
   * @param control
   * @returns {null|Promise<T>}
   */
  validate(control: FormControl): null|Promise<any> {
    return new Promise(resolve => {
        setTimeout(
          () => resolve(control.value === 'test' ? {message: 'Invalid value(directive)'} : null),
          1500
        );
    });
  }

}
