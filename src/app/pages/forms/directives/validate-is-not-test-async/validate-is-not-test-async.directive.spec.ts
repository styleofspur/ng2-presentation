/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { ValidateIsNotTestAsyncDirective } from './validate-is-not-test-async.directive';

describe('Directive: ValidateIsNotTestAsync', () => {
  it('should create an instance', () => {
    let directive = new ValidateIsNotTestAsyncDirective();
    expect(directive).toBeTruthy();
  });
});
