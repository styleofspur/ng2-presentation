/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { ValidateIsNotTestDirective } from './validate-is-not-test.directive';

describe('Directive: ValidateIsNotTest', () => {
  it('should create an instance', () => {
    let directive = new ValidateIsNotTestDirective();
    expect(directive).toBeTruthy();
  });
});
