import { Directive }                  from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appValidateIsNotTest]',
  providers: [
    // with multi providers, we can provide multiple values for a single token in DI
    { provide: NG_VALIDATORS, useExisting: ValidateIsNotTestDirective, multi: true}
  ]
})
export class ValidateIsNotTestDirective {

  constructor() { }

  /**
   * In order to make this method work on each form control change we've provided NG_VALIDATORS above
   *
   * @param {FormControl} control
   * @returns {{message: string}|null}
   */
  validate(control: FormControl): null|{message: string} {
    return control.value === 'test' ? {message: 'Invalid value(directive)'} : null;
  }

}
