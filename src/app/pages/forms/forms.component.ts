import {
  Component, ViewChild,
  ElementRef, AfterViewInit,
  Renderer
} from '@angular/core';
import { ActivatedRoute }  from '@angular/router';
import { NgModel, NgForm } from '@angular/forms';
import { Title }           from '@angular/platform-browser';

import { Page }  from '../page.class';
import { Model } from './models/model.class';

@Component({
  selector:    'app-forms',
  templateUrl: './forms.component.html',
  styleUrls:   ['./forms.component.sass']
})
export class FormsComponent extends Page implements AfterViewInit {

  /**
   * @type {Renderer}
   * @private
   */
  private _renderer: Renderer = null;

  /**
   * @type {ElementRef}
   * @private
   */
  @ViewChild('stateAndValidityTrackingInput')
  private _stateAndValidityTrackingInput: ElementRef;

  /**
   * @type {NgModel}
   * @private
   */
  @ViewChild('showHideValidationMessagesInput')
  private _showHideValidationMessagesInput: NgModel;

  /**
   * @type {NgForm}
   * @private
   */
  @ViewChild('ngFormApiForm')
  private _ngFormApiFrom: NgForm;

  /**
   * @public
   * @type {string}
   */
  public text: string = 'Forms';

  /**
   * @public
   * @type {Model}
   */
  public model: Model = null;

  /**
   * @constructor
   * @param {ActivatedRoute} route
   * @param {Title}          title
   * @param {Renderer}       renderer
   */
  constructor(route?: ActivatedRoute, title?: Title, renderer?: Renderer) {
    super(route, title);

    this._renderer = renderer;
    this.model     = new Model();
  }

  public ngAfterViewInit() {
    this._renderer.invokeElementMethod(
        this._stateAndValidityTrackingInput.nativeElement, 'focus'
    );

    console.log('State & Validity Tracking Input', this._stateAndValidityTrackingInput);
    console.log('Show/Hide Validation Messages Input', this._showHideValidationMessagesInput);
  }

  /**
   * @public
   * @param {Event} event
   */
  public onSubmit(event: Event) {
      event.preventDefault();

      console.log(this._ngFormApiFrom);
  }

}
