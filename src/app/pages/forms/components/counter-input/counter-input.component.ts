import { Component, Input, forwardRef, OnChanges } from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS
} from '@angular/forms';
import { isUndefined } from 'util';

export function createCounterRangeValidator(maxValue: number, minValue: number): Function {

  return function validateCounterRange(control: FormControl): Object|null {
    const err = {
      rangeError: {
        given: control.value,
        max:   maxValue || 10,
        min:   minValue || 0
      }
    };

    return (control.value > +maxValue || control.value < +minValue) ? err : null;
  };

}

/*

  ControlValueAccessor is the thing that Angular uses to build a bridge between a form model and a DOM element.

  While our counter component works, there’s currently no way we can connect it to an outer form.
  If we try to bind any kind of form model to our custom control, we’ll get an error that there’s a missing ControlValueAccessor.

  One of the things we need to make sure is that changes are propagated from the model to the view/DOM, and also from the view, back to the model.
  This is what a ControlValueAccessor is for.

  A ControlValueAccessor is an interface that takes care of:
    * Writing a value from the form model into the view/DOM
    * Informing other form directives and controls when the view/DOM changes

 The reason why Angular has such an interface, is because the way how DOM elements need to be updated can vary across input types.
 For example, a normal text input has a value property that needs to be written to, whereas a checkbox comes with a checked property that needs to be updated.

  If we take a look under the hood, we see that there’s a ControlValueAccessor for every input type which knows how to update its view/DOM.
  There’s
    * the DefaultValueAccessor that takes care of text inputs and textareas,
    * the SelectControlValueAccessor that handles select inputs,
    * the CheckboxControlValueAccessor,
  and many more.

 Our counter component needs a ControlValueAccessor that knows how to update the _counterValue model and inform the outside world about changes too.

*/

@Component({
  selector:    'app-counter-input',
  providers: [
    /*

     We add our custom value accessor to the DI system so Angular can get an instance of it.
     We also have to use useExisting because CounterInputComponent will be already created as a directive dependency in the component that uses it.
     If we don’t do that, we get a new instance as this is how DI in Angular works.

    */
    {
      provide:     NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CounterInputComponent),
      multi:       true
    },
    {
      provide: NG_VALIDATORS,
      /*
        We can provide a value - so a function which performs validation on any input change:

          useValue: createCounterRangeValidator

        But in our case component itself performs validation, and we need correct instance of it. So we use the next approach.

      */
      useExisting: forwardRef(() => CounterInputComponent),
      multi:       true
    }
  ],
  templateUrl: './counter-input.component.html',
  styleUrls:   ['./counter-input.component.sass']
})
export class CounterInputComponent implements OnChanges, ControlValueAccessor {

  /**
   * @public
   * @type {number}
   */
  @Input()
  public _counterValue: number = 0;

  /**
   * @public
   * @type {number}
   */
  @Input()
  public counterRangeMax: number;

  /**
   * @public
   * @type {number}
   */
  @Input()
  public counterRangeMin: number;

  /**
   * @returnss {number}
   */
  get counterValue(): number {
    return this._counterValue;
  }

  /**
   * @param {number} value
   */
  set counterValue(value: number) {
    this._counterValue = value;
    this.propagateChange(value);
  }

  /**
   *
   * @param {any} value
   * @private
   * @returns void
   */
  private _validateFn: Function = (value: any): void => {};

  /**
   *
   * @public
   * @param {any} _
   * @returns void
   */
  public propagateChange: Function = (_: any): void => {}; // default value is a dumb callback

  /**
   * @public
   * @returnss void
   */
  public increment(): void {
    this._counterValue++;
    this.propagateChange(this._counterValue);
  }

  /**
   * @public
   * @returnss void
   */
  public decrement(): void {
    this._counterValue--;
    this.propagateChange(this._counterValue);
  }

  /**
   *  The method writes a new value from the form model into the view or (if needed) DOM property.
   *  This is where we want to update our _counterValue model, as that’s the thing that is used in the view.
   *
   *  It takes a new value from the form model and writes it into the view.
   *
   *  The method gets called when the form is initialized, with the form model’s initial value.
   *  It means it will override the default value 0, so we need to handle this.
   *
   * @public
   * @param {any} value
   * @returns void
   */
  public writeValue(value: any): void {
    console.log('writeValue()', value);
    if (!isUndefined(value)) {
      this._counterValue = value;
    }
  }

  /**
   * The method registers a handler that should be called when something in the view has changed.
   * It gets a function that tells other form directives and form controls to update their values.
   * In other words, that’s the handler function we want to call whenever _counterValue changes through the view.
   *
   * It has access to a function that informs the outside world about changes.
   * Here’s where we can do special work, whenever we propagate the change, if we want to.
   *
   * @public
   * @param {any} fn
   * @returns void
   */
  public registerOnChange(fn: any): void {
    this.propagateChange = (value: any): void => {
      console.log(`<counter> propagates value change. OLD: ${this._counterValue}, NEW: ${value}`, );
      fn(value);
    };
  }

  /**
   * Similar to registerOnChange(), this registers a handler specifically for when a control receives a touch event.
   * We don’t need that in our custom control.
   *
   * It registers a callback that is excuted whenever a form control is “touched”.
   * E.g. when an input element blurs, it fire the touch event.
   * We don’t want to do anything at this event, so we can implement the interface with an empty function.
   *
   * @public
   * @param {any} fn
   * @returns void
   */
  public registerOnTouched(fn: any): any {
    console.log('<counter> has been touched');
  }

  /**
   * ValidateFn is only set in ngOnInit().
   * What if counterRangeMax or counterRangeMin change via their bindings?
   *
   * We need to create a new validator function based on these changes.
   * ngOnChanges() lifecycle hook that, allows us to do exactly that.
   * All we have to do is to check if there are changes on one of the input properties and recreate our validator function.
   * We can even get rid off ngOnInit() again, because ngOnChanges() is called before ngOnInit() anyways.
   *
   * @public
   * @param {Object} changes
   * @returns void
   */
  public ngOnChanges(changes): void {
    if (changes.counterRangeMin || changes.counterRangeMax) {
      this._validateFn = createCounterRangeValidator(this.counterRangeMax, this.counterRangeMin);
      this.propagateChange(this._counterValue); // run validation immediately after changes of range values
    }
  }

  /**
   * Angular uses it to perform validation.
   *
   * @param {FormControl} control
   * @returnss {Object|null}
   */
  public validate(control: FormControl): Object|null {
      return this._validateFn(control);
  }

}
