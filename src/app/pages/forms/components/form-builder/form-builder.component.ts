import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder }           from '@angular/forms';

import { FormBuilderModel } from '../../models/form-builder.class';

@Component({
  selector:    'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls:   ['./form-builder.component.sass']
})
export class FormBuilderComponent implements OnInit, AfterViewInit {

  form: FormGroup;

  /**
   * @void
   * @private
   */
  private _aggregateFormErrors(): void {
    const errors: Object = {};
    for (let control in this.form.controls) {
      if (this.form.controls.hasOwnProperty(control)) {
        errors[control] = this.form.controls[control].errors;
      }
    }

    this.form.setErrors(errors);
  }

  /**
   * @constructor
   */
  constructor(formBuilder: FormBuilder) {
    /*
     FormGroup instance represents a <form> or a group of form's inputs.
     FormGroups can be nested.

      example:

     registerForm = new FormGroup({
        firstname: new FormControl(defaultValue, syncValidators, asyncValidators),
        lastname: new FormControl(),
        address: new FormGroup({
          street: new FormControl(),
          zip: new FormControl(),
          city: new FormControl()
        })
     });

    */
    this.form = formBuilder.group(new FormBuilderModel());
  }

  ngAfterViewInit(): void {
    this._aggregateFormErrors();
  }

  ngOnInit(): void {
    /*
     We can listen reactively for changes that are happening to the form/control.
     Every form controls exposes an Observable property valuesChanges() that we can subscribe to.
    */
    this.form.valueChanges.subscribe(values => {
      console.log('VALUES', values);
      this._aggregateFormErrors();
    });
  }

}
