import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormsComponent } from './forms.component';

const rootRouteConfig = {
  path: 'forms',
  component: FormsComponent,
  data: {
    title: 'Forms Page'
  }
};
const routes: Routes = [
  rootRouteConfig
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule { }
