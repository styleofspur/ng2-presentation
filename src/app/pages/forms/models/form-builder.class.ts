import { Validators, FormControl } from '@angular/forms';

export class FormBuilderModel {

    /**
     * @public
     * @type {string}
     */
    public requiredField: string = '';

    /**
     * @public
     * @type {string}
     */
    public patternField: string = '';

    /**
     * @public
     * @type {string}
     */
    public minMaxLengthField = [
        'aaa', // default value
        Validators.compose([Validators.minLength(2), Validators.maxLength(5)])
    ];

    /**
     * @public
     * @type {FormControl}
     */
    public customField = new FormControl('',
        // sync validator(s)
        control => {
            return control.value === 'test' ? {message: 'Invalid value'} : null;
        }
    );

    /**
     * @public
     * @type {FormControl}
     */
    public customAsyncField = new FormControl('',
        // sync validator(s)
        null,
        // async validator(s)
        control => {
            return new Promise(resolve => {
                setTimeout(
                    () => resolve(control.value === 'test' ? {message: 'Invalid value (async)'} : null),
                    1500
                );
            });
        }
    );

    /**
     * @public
     * @type {string}
     */
    public validatorDirectiveField: string = '';

    /**
     * @public
     * @type {string}
     */
    public validatorDirectiveAsyncField: string = '';

}
