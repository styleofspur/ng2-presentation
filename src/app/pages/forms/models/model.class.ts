export class Model {

    /**
     * @public
     * @type {string}
     */
    public stateAndValidityTracking: string = '';

    /**
     * @public
     * @type {string}
     */
    public showHideValidationMessages: string = '';

    /**
     * @public
     * @type {{name: string; password: string; disabled: boolean}}
     */
    public ngFormApi = {
        name:     <string>  '',
        password: <string>  '',
        disabled: <boolean> false
    };

    /**
     * @public
     * @type {{counter: number}}
     */
    public custom = {
        counter:  <number> 3,
        minRange: <number> 2,
        maxRange: <number> 5
    };
}
