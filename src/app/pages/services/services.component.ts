import { Component, Optional, Injector } from '@angular/core';
import { ActivatedRoute }                from '@angular/router';
import { Title }                         from '@angular/platform-browser';

import { ServicesSpecificService }    from './services/services-specific/services-specific.service';
import { EverywhereAvailableService } from '../../services/everywhere-available/everywhere-available.service';
import { Page }                       from '../page.class';


/**
 * Component classes should be lean.
 * They don't fetch data from the server, validate user input, or log directly to the console.
 * They delegate such tasks to services.
 *
 * A component's job is to enable the user experience and nothing more.
 * It mediates between the view (rendered by the template) and the application logic (which often includes some notion of a model).
 * A good component presents properties and methods for data binding.
 * It delegates everything nontrivial to services.
 */
@Component({
  selector:    'app-services',
  /* DI */
  providers:   [
      /*
        A shorthand expression for a provider registration using object literal with 2 props:

          [{ provide: ServicesSpecificService, useClass: ServicesSpecificService // or another class }]

          * "provide" is the token that serves as the key for both locating a dependency value and registering the provider.
          * "useClass" is a provider definition object, which we can think of as a recipe for creating the dependency value.

       - Value providers
          Sometimes it's easier to provide a ready-made object rather than ask the injector to create it from a class.

          [{ provide: ServicesSpecificService, useValue: {data: ['a', 'b' 'c']} }] // TypeScript interfaces aren't valid tokens in useValue

       - Factory providers
          Sometimes we need to create the dependent value dynamically, based on information we won't have until the last possible moment.
          Maybe the information changes repeatedly in the course of the browser session.

          {
            provide: ServicesSpecificService,
            useFactory: ServicesSpecificServiceFactory,
            deps: [FirstDepService, SecondDepService]
          }

          * "useFactory" tells Angular that the provider is a factory function whose implementation is the heroServiceFactory.
          * "deps"  is an array of provider tokens. Its classes (items) serve as tokens for their own class providers.
              The injector resolves these tokens and injects the corresponding services into the matching factory function parameters.

       */
      ServicesSpecificService
  ],
  templateUrl: './services.component.html',
  styleUrls:   ['./services.component.sass']
})
export class ServicesComponent extends Page {

  public servicesSpecificService: ServicesSpecificService;

    /**
     * @type {string}
     * @public
     */
    public text: string = 'Services';

  /**
   *
   * This is where the dependency injection framework comes into play.
   * Imagine the framework had something called an injector.
   * We register some classes with this injector, and it figures out how to create them.

      When we need a ServicesSpecificService, we simply ask the injector to get it for us and we're good to go.

        let servicesSpecificService = injector.get(ServicesSpecificService);

   ---------------------------------------------------------------------------------------------------------------------

      1) for EverywhereAvailableService instantiation
        * the constructor parameter type
        * the @Component decorator
        * and the parent's providers information (array of "providers" inside app.module.ts)

      2) for ServicesSpecificService instantiation
        * the @Component decorator
        * the "providers" array option with ServicesSpecificService as its element in the @Component decorator

     combine to tell the Angular injector to inject an instance of necessary service whenever it creates a new HeroListComponent.

   * @constructor
   */
  constructor(
      /* share data between this component and all other parts of the app (one instance all over constructor param injection) */
      @Optional()
      public everywhereAvailableService: EverywhereAvailableService,
      @Optional()
      public servicesSpecificService1: ServicesSpecificService,
      @Optional()
      private injector: Injector,

      route?: ActivatedRoute,
      title?: Title
  ) {
    super(route, title);
    this.servicesSpecificService = this.injector.get(ServicesSpecificService);
    console.log(`
        servicesSpecificService1 === this.servicesSpecificService is 
        ${servicesSpecificService1 === this.servicesSpecificService}
    `);
  }

}
