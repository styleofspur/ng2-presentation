/* tslint:disable:no-unused-variable */

import { TestBed, async, inject }  from '@angular/core/testing';
import { ServicesSpecificService } from './services-specific.service';

describe('Service: ServicesSpecificService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServicesSpecificService]
    });
  });

  it('should ...', inject([ServicesSpecificService], (service: ServicesSpecificService) => {
    expect(service).toBeTruthy();
  }));
});
