import { Injectable } from '@angular/core';

@Injectable()
export class ServicesSpecificService {

  private _data: Array<string> = [
      'services', 'specific', 'service'
  ];

  /**
   * @public
   * @returns {Array<string>}
   */
  public get data(): Array<string> {
      return this._data;
  }

  /**
   * @param {Array<string>} value
   * @public
   */
  public set data(value: Array<string>) {
      this._data = value;
  }

}
