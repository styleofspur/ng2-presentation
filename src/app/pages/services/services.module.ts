import { NgModule } from '@angular/core';

import { ServicesComponent } from './services.component';

@NgModule({
  declarations: [
    ServicesComponent
  ],
  imports: [

  ],
  bootstrap: [ServicesComponent]
})
export class ServicesModule { }
