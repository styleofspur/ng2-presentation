/* tslint:disable:no-unused-variable */

import { TestBed, async }     from '@angular/core/testing';
import { HighlightDirective } from './highlight.directive';

describe('Directive: Highlight [appHighlight]', () => {

  describe('properties', () => {

    it('should create an instance', () => {
      let directive = new HighlightDirective();
      expect(directive).toBeTruthy();
    });

    it('should have appHighlight property with undefined value by default', () => {
      let directive = new HighlightDirective();
      expect(directive.appHighlight).toBeUndefined();
    });

    it('should have defaultColor property with "red" value by default', () => {
      let directive = new HighlightDirective();
      expect(directive.defaultColor).toBe('red');
    });

    it('should have settable defaultColor property', () => {
      let directive = new HighlightDirective();
      directive.defaultColor = 'yellow';
      expect(directive.defaultColor).toBe('yellow');
    });

  });

  describe('methods', () => {

    it('should have void onMouseEnter method', () => {
      let directive = new HighlightDirective();

      expect(directive.onMouseEnter).toBeDefined();
      expect(typeof directive.onMouseEnter).toBe('function');

      spyOn(directive, 'onMouseEnter');
      const result = directive.onMouseEnter();

      expect(result).toBeUndefined();
      expect(directive.onMouseEnter).toHaveBeenCalled();
      expect(directive.onMouseEnter).toHaveBeenCalledTimes(1);
      expect(directive.onMouseEnter).toHaveBeenCalledWith();
    });
    
    it('should have void onMouseLeave method', () => {
      let directive = new HighlightDirective();

      expect(directive.onMouseLeave).toBeDefined();
      expect(typeof directive.onMouseLeave).toBe('function');

      spyOn(directive, 'onMouseLeave');

      const result = directive.onMouseLeave();

      expect(result).toBeUndefined();
      expect(directive.onMouseLeave).toHaveBeenCalled();
      expect(directive.onMouseLeave).toHaveBeenCalledTimes(1);
      expect(directive.onMouseLeave).toHaveBeenCalledWith();
    });

  });

});
