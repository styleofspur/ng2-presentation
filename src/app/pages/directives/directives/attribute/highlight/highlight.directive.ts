import {
  Directive,
  ElementRef,

  /*
    The @HostListener decorator refers to the DOM element that hosts an attribute directive
  */
  HostListener,

  Input
} from '@angular/core';

/**
 * There are three kinds of directives in Angular:

  1. Components—directives with a template.
  2. Structural directives—change the DOM layout by adding and removing DOM elements.
  3. Attribute directives—change the appearance or behavior of an element.

 * Attribute directives are used as attributes of elements.
 * The built-in NgStyle directive in the Template Syntax page, for example, can change several element styles at the same time.
 */
@Directive({

  /*
    Specifies a CSS selector that identifies this directive within a template.
    Supported selectors include element, [attribute], .class, and :not().
    Does not support parent-child relationship selectors.

    We do not prefix our unless directive name with ng.
    That prefix belongs to Angular and we don't want to confuse our directives with their directives.

    Our prefix is "app"
  */
  selector: '[appHighlight]',

  /*
    List of dependency injection providers for this directive and its children.
  */
  providers: [/* services */],

  /*
    Map of class property to host element bindings for events, properties and attributes
  */
  // host: {
  //    'attr.role':    'button',        // sets attr-role="button" for host DOM node
  //    '(mouseenter)': 'onMouseEnter()' // sets directive's onMouseEnter() method as host DOM node's onmouseenter event listener
  // },

  /*

    Or it can be done with @HostBinding() annotations:

    @HostBinding('attr.role')
    role = 'button';

    @HostListener('mouseenter')
    onMouseEnter() {
      // do work
    }

  */

  /*
    List of class property names to data-bind as component inputs
  */
  // inputs: [ 'appHighlight', 'defaultColor' ],

  /*
    List of class property names that expose output events that others can subscribe to
  */
  // outputs: [ 'appHighlightChange' ],

  /*
    Configure queries that can be injected into the component
  */
  // queries: {
  //
  // },

  /*
    Name under which the component instance is exported in a template
  */
  // exportAs: 'appHighlight'
})
export class HighlightDirective {

  /**
   * @type {string}
   * @private
   */
  private _defaultColor: string = 'red';

  /**
   * @type ElementRef
   * @private
   */
  private _el: ElementRef;

  /**
   * @type {string}
   * @public
   */
  @Input('appHighlight') // [appHighlight]="<value>" -> <value> will be assigned to this property
  public appHighlight: string;

  /**
   * @type {string}
   * @param colorName
   */
  @Input(/* defaultColor */)
  set defaultColor(colorName: string) {
    this._defaultColor = colorName || this._defaultColor;
  }

  /**
   * @returns {string}
   */
  get defaultColor(): string {
    return this._defaultColor;
  }

  /**
   *
   * @private
   * @param {string} color
   * @void
   */
  private _highlight(color: string): void {
    this._el.nativeElement.style.backgroundColor = color;
  }

  /**
   * Remember that a directive's data-bound input properties are not set until after construction.
   * That's a problem if you need to initialize the directive based on those properties. They'll have been set when ngOninit runs.
   * The ngOnChanges method is your first opportunity to access those properties.
   *
   * @param {ElementRef} el
   */
  constructor(el?: ElementRef) {
    this._el = el;
  }

  /**
   * @public
   * @void
   */
  @HostListener('mouseenter')
  public onMouseEnter(): void {
    this._highlight(this.appHighlight || this._defaultColor);
  }

  /**
   * @public
   * @void
   */
  @HostListener('mouseleave')
  public onMouseLeave(): void {
    this._highlight(null);
  }

}
