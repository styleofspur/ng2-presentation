/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { UnlessDirective } from './unless.directive';

describe('Directive: Unless [appUnless]', () => {

  it('should create an instance', () => {
    let directive = new UnlessDirective();
    expect(directive).toBeTruthy();
  });

  it('should have appUnless property with undefined value by default', () => {
    let directive = new UnlessDirective();
    expect(directive.appUnless).toBeUndefined();
  });

});
