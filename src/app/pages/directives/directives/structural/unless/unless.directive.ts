import {
  Directive,
  Input,

  /*
   Represents an Embedded Template that can be used to instantiate Embedded Views.

   You can access a TemplateRef, in two ways.
   1) Via a directive placed on a <template> element (or directive prefixed with *)
   2) have the TemplateRef for this Embedded View injected into the constructor of the directive using the TemplateRef Token.
   Alternatively you can query for the TemplateRef from a Component or a Directive via Query.

   To instantiate Embedded Views based on a Template, use ViewContainerRef, which will create the View and attach it to the View Container.
  */
  TemplateRef,

  // to access the renderer
  ViewContainerRef
} from '@angular/core';

/**
 * There are three kinds of directives in Angular:

 1. Components—directives with a template.
 2. Structural directives—change the DOM layout by adding and removing DOM elements.
 3. Attribute directives—change the appearance or behavior of an element.

 * Structural directive changes the DOM layout by adding and removing DOM elements.
 * For example, structural directives are ngIf, ngSwitch and ngFor.
 */
@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {

  /**
   * @private
   * @type {TemplateRef<any>}
   */
  private _templateRef: TemplateRef<any>;

  /**
   * @private
   * @type {ViewContainerRef}
   */
  private _viewContainer: ViewContainerRef;

  /**
   *
   * Structural directives, like ngIf, use the HTML 5 <template> tag.
   *
   * Outside of an Angular app, the <template> tag's default CSS display property is none.
   * It's contents are invisible within a hidden document fragment.
   *
   * Inside of an app, Angular removes the <template> tags and their children.
   * The contents are gone — but not forgotten.
   *
   * @param {boolean} condition
   */
  @Input()
  set appUnless(condition: boolean) {

    // when the condition is true, Angular inserts the template's content into the DOM.
    if (!condition) {
      this._viewContainer.createEmbeddedView(this._templateRef);
    } else {
      this._viewContainer.clear();
    }
  }

  /**
   * Remember that a directive's data-bound input properties are not set until after construction.
   * That's a problem if you need to initialize the directive based on those properties. They'll have been set when ngOninit runs.
   * The ngOnChanges method is your first opportunity to access those properties.
   *
   * @param {TemplateRef<any>} templateRef
   * @param {ViewContainerRef} viewContainer
   */
  constructor(templateRef?: TemplateRef<any>, viewContainer?: ViewContainerRef) {
    this._templateRef   = templateRef;
    this._viewContainer = viewContainer;
  }

}
