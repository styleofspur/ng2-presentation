import { Component }      from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title }          from '@angular/platform-browser';

import { Page } from '../page.class';

@Component({
  selector:    'app-directives-page',
  templateUrl: './directives.component.html',
  styleUrls:   ['./directives.component.sass']
})
export class DirectivesComponent extends Page {

  /**
   * @type {string}
   * @protected
   */
  protected _title: string = 'directives page';

  /**
   * @type {string}
   * @public
   */
  public text: string = 'Directives';

  /**
   *
   * @constructor
   * @param {ActivatedRoute} route
   * @param {Title} title
   */
  constructor(route?: ActivatedRoute, title?: Title) {
    super(route, title);
  }

}
