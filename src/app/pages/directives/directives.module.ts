import { NgModule } from '@angular/core';

import { DirectivesComponent } from './directives.component';
import { HighlightDirective }  from './directives/attribute/highlight/highlight.directive';
import { UnlessDirective }     from './directives/structural/unless/unless.directive';

@NgModule({
  declarations: [
    // components
    DirectivesComponent,

    // directives
    HighlightDirective,
    UnlessDirective
  ],
  imports: [

  ],
  bootstrap: [DirectivesComponent]
})
export class DirectivesModule { }
