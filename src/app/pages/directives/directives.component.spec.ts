/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Component }              from '@angular/core';
import { Location }               from '@angular/common';
import { Router, Route }          from '@angular/router';
import { RouterTestingModule }    from '@angular/router/testing';

import { DirectivesComponent }      from './directives.component';
import { HighlightDirective }       from './directives/attribute/highlight/highlight.directive';
import { UnlessDirective }          from './directives/structural/unless/unless.directive';

@Component({
  template: `
    <router-outlet></router-outlet>
  `
})
class RoutingComponent { }

describe('DirectivesComponent', () => {

  describe('render', () => {

    let router, location;
    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [
          RoutingComponent,
          DirectivesComponent,

          HighlightDirective,
          UnlessDirective
        ],
        imports: [
          RouterTestingModule.withRoutes([
            { path: 'directives', component: DirectivesComponent, data: { title: 'Directives Page'} }
          ])
        ]
      });
    });

    beforeEach(inject([Router, Location], (_router: Router, _location: Location) => {
      location = _location;
      router   = _router;
    }));

    it('should create the directives page', async(() => {
      let fixture = TestBed.createComponent(DirectivesComponent);
      let app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    }));

    it(`should have as title 'Directives'`, async(() => {
      let fixture = TestBed.createComponent(DirectivesComponent);
      let app = fixture.debugElement.componentInstance;
      expect(app.text).toEqual('Directives');
    }));

    it('should set page title with default value', async(() => {
      let fixture = TestBed.createComponent(DirectivesComponent);
      fixture.detectChanges();
      expect(document.title).toBe('directives page');
    }));

    it('should set page title with router data value', async(() => {
      let fixture = TestBed.createComponent(RoutingComponent);
      fixture.detectChanges();
      router.navigate(['/directives']).then(() => {
        expect(location.path()).toBe('/directives');
        expect(document.title).toBe('Directives Page');
      });
    }));

  });

  describe('instance', () => {

    it('should have `text` property with string type and default value', () => {
      let instance = new DirectivesComponent();
      expect(instance.text).toBeDefined();
      expect(typeof instance.text).toBe('string');
      expect(instance.text).toBe('Directives');
    });

  });

});
