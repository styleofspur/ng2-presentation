import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DirectivesComponent } from './directives.component';

const rootRouteConfig = {
  path: 'directives',
  component: DirectivesComponent,
  data: {
    title: 'Directives Page'
  }
};
const routes: Routes = [
  rootRouteConfig
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DirectivesRoutingModule { }
