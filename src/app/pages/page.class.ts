import { OnInit }         from '@angular/core';
import { Title }          from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

export class Page implements OnInit {

    /**
     * @type {string}
     * @protected
     */
    protected _title: string;

    /**
     * @type {ActivatedRoute}
     * @protected
     */
    protected _route: ActivatedRoute = null;

    /**
     * @constructor
     * @param {ActivatedRoute} route
     * @param {Title} _titleService
     */
    constructor(route: ActivatedRoute, private _titleService: Title) {
        this._route = route;
    };

    /**
     * Sets page title.
     *
     * @protected
     * @returns {void}
     */
    protected _setTitle(): void {
        this._route.data
            .subscribe((data: {title: string}) => {
                this._titleService.setTitle(data.title || this._title);
            });
    }

    /**
     * On component init hook.
     *
     * @public
     * @returns {void}
     */
    public ngOnInit(): void {
        this._setTitle();
    };
}
