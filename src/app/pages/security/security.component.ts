import { Component }                    from '@angular/core';
import { ActivatedRoute }               from '@angular/router';
import { Title, DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { Page } from '../page.class';

@Component({
  selector:    'app-security',
  templateUrl: './security.component.html',
  styleUrls:  [ './security.component.sass' ]
})
export class SecurityComponent extends Page {

  /**
   * @public
   * @type {string}
   */
  public htmlSnippet: string = '<strong>Hi!</strong>';

  /**
   * @public
   * @type {string}
   */
  public dangerousUrl: string = 'javascript:alert(`Hi!`)';

  /**
   * @public
   * @type {string}
   */
  public trustedUrl: SafeUrl;

  /**
   * @constructor
   * @public
   * @param {DomSanitizer} sanitizer
   * @param {ActivatedRoute} route
   * @param {Title} title
   */
  public constructor(private sanitizer: DomSanitizer, route?: ActivatedRoute, title?: Title) {
    super(route, title);

    this.trustedUrl = sanitizer.bypassSecurityTrustUrl(this.dangerousUrl);
  }

}
