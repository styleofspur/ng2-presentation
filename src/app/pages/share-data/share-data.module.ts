import { NgModule } from '@angular/core';

import { ShareDataComponent } from './share-data.component';

@NgModule({
  declarations: [
    ShareDataComponent
  ],
  imports: [

  ],
  bootstrap: [ShareDataComponent]
})
export class ShareDataModule { }
