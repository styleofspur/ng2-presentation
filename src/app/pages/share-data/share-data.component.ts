import { Component }      from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title }          from '@angular/platform-browser';

import { Page } from '../page.class';

import { EverywhereAvailableService } from '../../services/everywhere-available/everywhere-available.service';

@Component({
  selector:    'app-share-data-page',
  templateUrl: './share-data.component.html',
  styleUrls:   ['./share-data.component.sass']
})
export class ShareDataComponent extends Page {

  /**
   * @type {string}
   * @protected
   */
  protected _title: string = 'share data page';

  /**
   * @public
   * @type {string}
   */
  public text: string = 'Share Data';

  /**
   * @constructor
   * @param {ActivatedRoute} route
   * @param {Title}          title
   * @param {EverywhereAvailableService} everywhereAvailableService
   */
  constructor(route?: ActivatedRoute, title?: Title, public everywhereAvailableService?: EverywhereAvailableService ) {
    super(route, title);
  }

}
