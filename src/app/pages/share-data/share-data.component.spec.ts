/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Component }              from '@angular/core';
import { Location }               from '@angular/common';
import { Router }                 from '@angular/router';
import { RouterTestingModule }    from '@angular/router/testing';

import { ShareDataComponent } from './share-data.component';

@Component({
  template: `
    <router-outlet></router-outlet>
  `
})
class RoutingComponent { }

describe('ShareDataComponent', () => {

  describe('render', () => {

    let router, location;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [
          RoutingComponent,
          ShareDataComponent
        ],
        imports: [
          RouterTestingModule.withRoutes([
            { path: 'share-data', component: ShareDataComponent, data: { title: 'Share Data Page'} }
          ])
        ]
      });
    });

    beforeEach(inject([Router, Location], (_router: Router, _location: Location) => {
      location = _location;
      router   = _router;
    }));

    it('should create the Share Datapage', async(() => {
      let fixture = TestBed.createComponent(ShareDataComponent);
      let app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    }));

    it(`should have as title 'Share Data'`, async(() => {
      let fixture = TestBed.createComponent(ShareDataComponent);
      let app = fixture.debugElement.componentInstance;
      expect(app.text).toEqual('Share Data');
    }));

    it('should set page title with default value', async(() => {
      let fixture = TestBed.createComponent(ShareDataComponent);
      fixture.detectChanges();
      expect(document.title).toBe('share data page');
    }));

    it('should set page title with router data value', async(() => {
      let fixture = TestBed.createComponent(RoutingComponent);
      fixture.detectChanges();
      router.navigate(['/share-data']).then(() => {
        expect(location.path()).toBe('/share-data');
        expect(document.title).toBe('Share Data Page');
      });
    }));

  });

  describe('instance', () => {

    it('should have `text` property with string type and default value', () => {
      let instance = new ShareDataComponent();
      expect(instance.text).toBeDefined();
      expect(typeof instance.text).toBe('string');
      expect(instance.text).toBe('Share Data');
    });

  });

});
