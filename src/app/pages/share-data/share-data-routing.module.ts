import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShareDataComponent } from './share-data.component';

const rootRouteConfig = {
  path: 'share-data',
  component: ShareDataComponent,
  data: {
    title: 'Share Data Page'
  }
};
const routes: Routes = [
  rootRouteConfig
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ShareDataRoutingModule { }
