import {
    SimpleChanges,

    OnChanges,
    OnInit,
        DoCheck,
        AfterContentInit,
        AfterContentChecked,
        AfterViewInit,
        AfterViewChecked,
    OnDestroy,
} from '@angular/core';

/**
 * A component has a lifecycle managed by Angular itself.
 * Angular:
 *    1) creates it
 *    2) renders it
 *    3) creates and renders its children
 *    4) checks it when its data-bound properties change
 *    5) destroys it before removing it from the DOM
 *
 * Angular offers lifecycle hooks that provide visibility into these key life moments and the ability to act when they occur.
 * A directive has the same set of lifecycle hooks, minus the hooks that are specific to component content and views.
 *
 * @url https://angular.io/docs/ts/latest/guide/lifecycle-hooks.html#!#lifecycle-exercises
 */
export abstract class Lifecycle implements OnChanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

    /**
     * @protected
     * @type any
     */
    protected _viewChild: any = null;

    /**
     * @protected
     * @type any
     */
    protected _contentChild: any = null;

    /**
     * @type {object}
     * @public
     */
    public consoleColors = {
        constructor:           '#ff0066',
        ngOnChanges:           '#3366ff',
        ngOnInit:              '#009933',
        ngDoCheck:             '#3399ff',
        ngAfterContentInit:    '#cc00ff',
        ngAfterContentChecked: '#9900ff',
        ngAfterViewInit:       '#ff9933',
        ngAfterViewChecked:    '#cc6600',
        ngOnDestroy:           '#cc0000',

    };

    /**
     * The children elements which are located inside of the template of a component are called *view children*
     *
     * @ViewChild looks for elements in Shadow DOM
     * (an internal DOM of your component that is defined by you (as a creator of the component) and hidden from an end-user)
     * @ViewChild and  @ViewChildren only look for the elements that are on your the template directly.
     *
     * @public
     * @returns {any}
     */
    public get viewChild(): any {
        return this._viewChild || '';
    }

    /**
     * Elements which are used between the opening and closing tags of the host element of a given component are called *content children*
     * @ContentChild looks for elements in Light DOM
     * (a DOM that an end-user of your component supply into your component).
     * @ContentChild and @ContentChildren queries will return directives existing inside the <ng-content></ng-content> element of the view.
     * It's the same as a TRANSCLUSION in Angular 1.*
     *
     * @public
     * @returns {any}
     */
    public get contentChild(): any {
        return this._contentChild || '';
    }

    /**
     * @public
     * @type {string}
     */
    public value: string;

    /**
     * After creating a component/directive by calling its constructor, Angular calls the lifecycle hook methods.
     *
     * Don't fetch data in a component constructor.
     * You shouldn't worry that a new component will try to contact a remote server when created under test or before you decide to display it.
     * Constructors should do no more than set the initial local variables to simple values.
     *
     * Remember also that a directive's data-bound input properties are not set until after construction.
     * That's a problem if you need to initialize the directive based on those properties. They'll have been set when ngOnInit runs.
     * The ngOnChanges method is your first opportunity to access those properties.
     */
    constructor() {
        console.log(`%c${this} constructor`, `color: ${this.consoleColors.constructor}`, this.viewChild, this.contentChild);
    }

    /**
     * @public
     * @returns {string}
     */
    public abstract toString(): string;

    /**
     * Respond when Angular (re)sets data-bound input properties.
     * The method receives a SimpleChanges object of current and previous property values.
     * Called before ngOnInit and whenever one or more data-bound input properties change.
     *
     * @public
     * @param {SimpleChanges} changes
     * @returns void
     */
    public ngOnChanges(changes: SimpleChanges): void {
        console.log(`%c${this}.onChanges`, `color: ${this.consoleColors.ngOnChanges}`, changes, this.viewChild, this.contentChild);
    }

    /**
     * Initialize the directive/component after Angular first displays the data-bound properties and sets the directive/component's input properties.
     * Called once, after the first ngOnChanges.
     *
     * Use ngOnInit for two main reasons:
     *  1) to perform complex initializations shortly after construction
     *  2) to set up the component after Angular sets the input properties
     *
     * An ngOnInit is a good place for a component to fetch its initial data.
     *
     * @public
     * @returns void
     */
    public ngOnInit(): void {
        console.log(`%c${this}.onInit`, `color: ${this.consoleColors.ngOnInit}`, this.viewChild, this.contentChild);
        if (this.contentChild) {
            this.contentChild.value = this.value + Date.now();
            console.log(`${this}.contentChild.value`, this.contentChild.value);
        }
    }

    /**
     * Detect and act upon changes that Angular can't or won't detect on its own.
     * Called during every change detection run, immediately after ngOnChanges and ngOnInit.
     *
     * Use the DoCheck hook to detect and act upon changes that Angular doesn't catch on its own.
     * Use this method to detect a change that Angular overlooked.
     *
     * @public
     * @returns void
     */
    public ngDoCheck(): void {
        console.log(`%c${this}.doCheck`, `color: ${this.consoleColors.ngDoCheck}`);
        if (this.contentChild) {
            this.contentChild.value = this.value + Date.now();
            console.log(`${this}.contentChild.value`, this.contentChild.value);
        }
    }

    /**
     * Respond after Angular projects external content into the component's view.
     * Called once after the first ngDoCheck.
     *
     * Content projection is a way to import HTML content from outside the component and insert that content into the component's template in a designated spot.
     * Angular 1 developers know this technique as transclusion.
     *
     * A component-only hook.
     *
     * @public
     * @returns void
     */
    public ngAfterContentInit(): void {
        console.log(`%c${this}.afterContentInit`, `color: ${this.consoleColors.ngAfterContentInit}`, this.contentChild);
        if (this.contentChild) {
            this.contentChild.value = this.value + Date.now();
            console.log(`${this}.contentChild.value`, this.contentChild.value);
        }
    }

    /**
     * Respond after Angular checks the content projected into the component.
     * Called after the ngAfterContentInit and every subsequent ngDoCheck.
     *
     * Content projection is a way to import HTML content from outside the component and insert that content into the component's template in a designated spot.
     * Angular 1 developers know this technique as transclusion.
     *
     * A component-only hook.
     *
     * @public
     * @returns void
     */
    public ngAfterContentChecked(): void {
        console.log(`%c${this}.afterContentChecked`, `color: ${this.consoleColors.ngAfterContentChecked}`, this.contentChild);
        if (this.contentChild) {
            this.contentChild.value = this.value + Date.now();
            console.log(`${this}.contentChild.value`, this.contentChild.value);
        }
    }

    /**
     * Respond after Angular initializes the component's views and child views.
     * Called once after the first ngAfterContentChecked.
     *
     * A component-only hook.
     *
     * @public
     * @returns void
     */
    public ngAfterViewInit(): void {
        console.log(`%c${this}.afterViewInit`, `color: ${this.consoleColors.ngAfterViewInit}`, this.viewChild, this.contentChild);
        if (this.contentChild) {
            this.contentChild.value = this.value + Date.now();
            console.log(`${this}.contentChild.value`, this.contentChild.value);
        }
    }

    /**
     * Respond after Angular checks the component's views and child views.
     * Called after the ngAfterViewInit and every subsequent ngAfterContentChecked.
     *
     * A component-only hook.
     *
     * @public
     * @returns void
     */
    public ngAfterViewChecked(): void {
        console.log(`%c${this}.afterViewChecked`, `color: ${this.consoleColors.ngAfterViewChecked}`, this.viewChild, this.contentChild);

        /*
            This throws an exception.
            Angular's unidirectional data flow rule forbids updates to the view after it has been composed.
            Both of these hooks fire after the component's view has been composed.

            Angular throws an error if the hook updates the component's data-bound comment property immediately.
        */
        // if (this.contentChild) {
        //     this.contentChild.value = this.value + Date.now();
        //     console.log(`${this}.contentChild.value`, this.contentChild.value);
        // }
    }

    /**
     * Cleanup just before Angular destroys the directive/component.
     * Unsubscribes observables and detach event handlers to avoid memory leaks.
     * Called just before Angular destroys the directive/component.
     *
     * Put cleanup logic in ngOnDestroy, the logic that must run before Angular destroys the directive.
     *
     * This is the time to notify another part of the application that the component is going away.
     * This is the place to free resources that won't be garbage collected automatically.
     *    * Unsubscribe from observables and DOM events.
     *    * Stop interval timers.
     *    * Unregister all callbacks that this directive registered with global or application services.
     *
     * You risk memory leaks if you neglect to do so.
     *
     * @public
     * @returns void
     */
    public ngOnDestroy(): void {
        console.log(`%c${this}.onDestroy`, `color: ${this.consoleColors.ngOnDestroy}`, this.viewChild, this.contentChild);
    }

}
