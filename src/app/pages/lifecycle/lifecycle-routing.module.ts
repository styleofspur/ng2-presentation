import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LifecycleComponent } from './components/lifecycle/lifecycle.component';

const rootRouteConfig = {
  path: 'lifecycle',
  component: LifecycleComponent,
  data: {
    title: 'Lifecycle Page'
  }
};
const routes: Routes = [
  rootRouteConfig
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class LifecycleRoutingModule { }
