import { Component, Input, ContentChild } from '@angular/core';

import { Lifecycle }                      from '../../lifecycle.class';
import { ContentChildExampleComponent }   from '../content-child-example/content-child-example.component';

@Component({
  selector:    'app-lifecycle-view-child-example',
  templateUrl: './view-child-example.component.html',
  styleUrls:   [ './view-child-example.component.sass' ]
})
export class ViewChildExampleComponent extends Lifecycle {

  /**
   * @public
   * @type {string}
   */
  @Input()
  public value: string;

  /**
   * @protected
   * @type {ContentChildExampleComponent}
   */
  @ContentChild(ContentChildExampleComponent)
  protected _contentChild: ContentChildExampleComponent;

  /**
   * @public
   * @returns {string}
   */
  public toString(): string {
    return `#${ViewChildExampleComponent.name}`;
  }

}
