import { Component, Input } from '@angular/core';

import { Lifecycle } from '../../lifecycle.class';

@Component({
  selector:    'app-lifecycle-content-child-example',
  templateUrl: './content-child-example.component.html',
  styleUrls:   [ './content-child-example.component.sass' ]
})
export class ContentChildExampleComponent extends Lifecycle {

  /**
   * @public
   * @type {string}
   */
  @Input()
  public value: string;

  /**
   * @public
   * @returns {string}
   */
  public toString(): string {
    return `#${ContentChildExampleComponent.name}`;
  }

}
