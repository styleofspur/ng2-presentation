import { Component, ViewChild }      from '@angular/core';

import { Lifecycle }                 from '../../lifecycle.class';
import { ViewChildExampleComponent } from '../view-child-example/view-child-example.component';

@Component({
  selector:    'app-lifecycle',
  templateUrl: './lifecycle.component.html',
  styleUrls:   [ './lifecycle.component.sass' ]
})
export class LifecycleComponent extends Lifecycle {

  /**
   * @public
   * @type {string}
   */
  public model: string = 'test';

  /**
   * @protected
   * @type {ViewChildExampleComponent}
   */
  @ViewChild(ViewChildExampleComponent)
  protected _viewChild: ViewChildExampleComponent;

  /**
   * @public
   * @returns {string}
   */
  public toString(): string {
    return `#${LifecycleComponent.name}`;
  }

}
