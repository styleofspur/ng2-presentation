import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { LifecycleComponent }           from './components/lifecycle/lifecycle.component';
import { ViewChildExampleComponent }    from './components/view-child-example/view-child-example.component';
import { ContentChildExampleComponent } from './components/content-child-example/content-child-example.component';

@NgModule({
  declarations: [
      LifecycleComponent,
      ViewChildExampleComponent,
      ContentChildExampleComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  bootstrap: [LifecycleComponent]
})
export class LifecycleModule { }
