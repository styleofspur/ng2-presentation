import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { TemplateComponent } from './template.component';

@NgModule({
  declarations: [
    TemplateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  bootstrap: [TemplateComponent]
})
export class TemplateModule { }
