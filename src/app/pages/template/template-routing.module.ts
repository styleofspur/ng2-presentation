import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemplateComponent } from './template.component';

const rootRouteConfig = {
  path: 'template',
  component: TemplateComponent,
  data: {
    title: 'Template Page'
  }
};
const routes: Routes = [
  rootRouteConfig
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TemplateRoutingModule { }
