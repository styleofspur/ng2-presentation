import { Component }      from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title }          from '@angular/platform-browser';

import { Page } from '../page.class';

@Component({
  selector:    'app-template',
  templateUrl: './template.component.html',
  styleUrls:   ['./template.component.sass']
})
export class TemplateComponent extends Page {

  /**
   * @type {string}
   * @private
   */
  protected _title: string = 'template page';

  /**
   * @type {string}
   * @public
   */
  public text: string = 'Template';

  /**
   * @type {string}
   * @public
   */
  public interpolation = {
    expressions: {
      property: <string>   'Text',
      method:   <Function> (): string => 'hello',
    },
    bindings: {
      property: {
        element:   <string>  'https://angular.io/assets/images/logos/angular/logo-nav@2x.png',
        component: <string>  'test',
        directive: <boolean> true
      },
      event: {
        element:   <Function> (): void => alert('Clicked'),
        component: <Function> (): void => {},
        directive: <Function> (): void => {},
      },
      twoWay:    <string>  '',
      attribute: <string>  'Some text',
      class:     <boolean> true,
      style:     <string>  'red',
    }
  };

  /**
   * @constructor
   * @param {ActivatedRoute} route
   * @param {Title}          title
   */
  constructor(route?: ActivatedRoute, title?: Title) {
    super(route, title);
  }

  /**
   * @public
   * @returns {string}
   */
  public getInterpolationValue(): string {
    return 'hello!';
  }
}
