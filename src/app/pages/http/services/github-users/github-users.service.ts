import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';

@Injectable()
export class GithubUsersService {

  /**
   * @type Object
   * @private
   */
  private _urls = {
    all: 'https://api.github.com/users'
  };

  /**
   *
   * @param {Response} res
   * @returns {Object}
   * @private
   */
  private _extractData(res: Response): Object {
    const body = res.json();
    return body || [];
  }

  /**
   *
   * @param {Response|any} error
   * @returns {any}
   * @private
   */
  private _handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || '';
      const err  = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  /**
   * @public
   * @constructor
   * @param {Http} _http
   * @returns void
   */
  public constructor(private _http: Http) { }

  /**
   * @public
   * @returns {Observable<R>}
   */
  public getAll(): Observable<Object> {
    return this._http.get(this._urls.all)
                    .map(this._extractData)
                    .catch(this._handleError);
  }

}
