/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GithubUsersService } from './github-users.service';

describe('Service: GithubUsers', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GithubUsersService]
    });
  });

  it('should ...', inject([GithubUsersService], (service: GithubUsersService) => {
    expect(service).toBeTruthy();
  }));
});
