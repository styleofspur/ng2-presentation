import { NgModule }                   from '@angular/core';
import { CommonModule }               from '@angular/common';
import { HttpModule as NgHttpModule } from '@angular/http';

import { HttpComponent } from './http.component';

@NgModule({
  imports: [
    CommonModule,
    NgHttpModule
  ],
  declarations: [HttpComponent]
})
export class HttpModule { }
