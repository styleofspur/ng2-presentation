import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }    from '@angular/router';
import { Title }             from '@angular/platform-browser';

import { Page }               from '../page.class';
import { GithubUsersService } from './services/github-users/github-users.service';


@Component({
  selector:    'app-http',
  providers:   [
    GithubUsersService
  ],
  templateUrl: './http.component.html',
  styleUrls:   [ './http.component.sass' ]
})
export class HttpComponent extends Page implements OnInit {

  /**
   * @public
   * @type {any}
   */
  public githubUsers: any = [];

  /**
   * @public
   * @type {Object}
   */
  public errors = {
    all: null
  };

  /**
   * @public
   * @constructor
   * @param {GithubUsersService} _githubUsersService
   * @param {ActivatedRoute} route
   * @param {Title} title
   */
  public constructor(private _githubUsersService: GithubUsersService, route?: ActivatedRoute, title?: Title) {
    super(route, title);
  }

  /**
   * @public
   */
  public ngOnInit() {
    this._initUsers();
  }

  /**
   * @private
   */
  private _initUsers() {
    this._githubUsersService.getAll().subscribe(
        users => this.githubUsers = users,
        error => this.errors.all = error
    );
  }

  
}
