import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HttpComponent } from './http.component';

const rootRouteConfig = {
  path:      'http',
  component: HttpComponent,
  data: {
    title: 'HTTP Page'
  }
};
const routes: Routes = [
  rootRouteConfig
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class HttpRoutingModule { }
