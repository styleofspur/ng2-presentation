import { Component } from '@angular/core';

/**
 * A component controls a patch of screen called a "view".
 *
 * You define a component's application logic — what it does to support the view—inside a class.
 * It's responsible for exposing data to a view and handling most of the view’s display and user-interaction logic.
 * The class interacts with the view through an API of properties and methods.
 *
 * @Component extends @Directive
 */
@Component({

  /*
    If set, the templateUrl and styleUrl are resolved relative to the component.
   */
  // moduleId: module.id.toString(),

  /*
    CSS selector that tells Angular to create and insert an instance of this component
  */
  selector: 'app-root',

  /*
    Inline template or external template URL of the component's view.
   */
  // template: '<span>Hello, {{title}}</span>',
  templateUrl: './app.component.html',

  /*
    Inline template or external template URL of the component's view.
  */
  // styles: ['.app {color: red}'],
  styleUrls: ['./app.component.sass'],

  /*
    Array of dependency injection providers for services that the component requires.
  */
  providers: []
})
export class AppComponent {

  /**
   * @public
   * @type {string}
   */
  public title: string = 'app works!';

}
