import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DirectivesRoutingModule } from './pages/directives/directives-routing.module';
import { ShareDataRoutingModule }  from './pages/share-data/share-data-routing.module';
import { TemplateRoutingModule }   from './pages/template/template-routing.module';
import { ServicesRoutingModule }   from './pages/services/services-routing.module';
import { FormsRoutingModule }      from './pages/forms/forms-routing.module';
import { LifecycleRoutingModule }  from './pages/lifecycle/lifecycle-routing.module';
import { PipesRoutingModule }      from './pages/pipes/pipes-routing.module';
import { I18nRoutingModule }       from './pages/i18n/i18n-routing.module';
import { SecurityRoutingModule }   from './pages/security/security-routing.module';
import { HttpRoutingModule }       from './pages/http/http-routing.module';
import { ReduxRoutingModule }      from './pages/redux/redux-routing.module';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/directives',
        /*

         Redirect route requires a pathMatch property to tell the router how to match a URL to the path of a route.
         The router throws an error if you don't.
         In this app, the router should select the route to /directives only when the entire URL matches '', so set the pathMatch value to 'full'.

         Technically, pathMatch = 'full' results in a route hit when the remaining, unmatched segments of the URL match ''.
         In this example, the redirect is in a top level route so the remaining URL and the entire URL are the same thing.

         The other possible pathMatch value is 'prefix' which tells the router to match the redirect route when the remaining URL begins with the redirect route's prefix path.

         */
        pathMatch: 'full'
    }
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes),

      DirectivesRoutingModule, // directives page routing
      TemplateRoutingModule,   // template page routing
      ShareDataRoutingModule,  // share data page routing
      ServicesRoutingModule,   // services page routing
      FormsRoutingModule,      // forms page routing
      LifecycleRoutingModule,  // lifecycle page routing
      PipesRoutingModule,      // pipes page routing
      I18nRoutingModule,       // i18n page routing
      SecurityRoutingModule,   // security page routing
      HttpRoutingModule,       // http page routing
      ReduxRoutingModule,      // redux page routing
  ],
  exports:   [ RouterModule ],
  providers: []
})
export class AppRoutingModule { }
