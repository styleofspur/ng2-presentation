import { Injectable } from '@angular/core';

/**
 * Dependencies are singletons within the scope of an injector.
 * So a single EverywhereAvailableService instance is shared among all the AppModule's components.
 *
 * @Injectable() marks a class as available to an injector for instantiation.
 * An injector will report an error when trying to instantiate a class that is not marked as @Injectable().
 *
 * !!!
 * Always write @Injectable(), not just @Injectable.
 * Our application will fail mysteriously if we forget the parentheses.
 */
@Injectable()
export class EverywhereAvailableService {

  private _data: Array<string> = [
      'everywhere', 'available', 'service'
  ];

  /**
   * @public
   * @returns {Array<string>}
   */
  public get data(): Array<string> {
      return this._data;
  }

  /**
   * @public
   * @param {Array<string>} value
   */
  public set data(value: Array<string>) {
      this._data = value;
  }

}
