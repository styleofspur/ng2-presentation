/* tslint:disable:no-unused-variable */

import { TestBed, async, inject }     from '@angular/core/testing';
import { EverywhereAvailableService } from './everywhere-available.service';

describe('Service: EverywhereAvailable', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EverywhereAvailableService]
    });
  });

  it('should ...', inject([EverywhereAvailableService], (service: EverywhereAvailableService) => {
    expect(service).toBeTruthy();
  }));
});
